# randword.sh (https://bitbucket.org/matthewlinton/randword/)
Generate random words using aspell on the linux console.

## Instructions

1. Download the latest version (https://bitbucket.org/matthewlinton/randword/downloads).
1. Unzip the archive.
1. Navigate into the "randword" directory.
1. run "randword.sh" to start generating random words.

You can run "randword.sh -h" to get more options.

## Dependencies
randword requires the following software be installed on your system

### AWK (Required)
https://www.gnu.org/software/gawk/

### aspell (Required)
http://aspell.net/

### Festival (Optional)
http://www.cstr.ed.ac.uk/projects/festival/

## License
This code is not covered under any license. You are welcome to modify and share this code as you please.

## Liability
Use randword at your own risk!

randword, to the best of my knowledge, does not contain any harmful or malicious code. I assume no liability for any issues that may occur from the use of this software. Please take the time to understand how this code will interact with your system before using it.
